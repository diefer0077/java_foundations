public class ejercicio3 {
    public static int ackermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return ackermann(m - 1, 1);
        } else {
            return ackermann(m - 1, ackermann(m, n - 1));
        }
    }
    
    public static void main(String[] args) {
        int n = 3;
        int resultado = ackermann(3, n);
        System.out.println("El " + n + "-ésimo término de la secuencia de Ackermann es: " + resultado);
    }
}
