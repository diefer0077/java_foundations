import java.util.Scanner;
public class ejercicio4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el número total de elementos: ");
        int n = input.nextInt();

        System.out.print("Ingrese el número de elementos que se van a permutar: ");
        int k = input.nextInt();

        int permutations = factorial(n) / factorial(n - k);
        System.out.println("El número de permutaciones es: " + permutations);
    }

    public static int factorial(int num) {
        if (num <= 1) {
            return 1;
        } else {
            return num * factorial(num - 1);
        }
    }
}
