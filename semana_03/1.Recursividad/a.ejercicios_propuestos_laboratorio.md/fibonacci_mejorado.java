import java.util.Scanner;
import java.math.BigInteger;

public class fibonacci_mejorado {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);

        System.out.print("Indique el limite de la funcion de fibonacci: ");
        int n = leer.nextInt();
        for(int i = 0 ; i <= n ; i++){
            System.out.println(">> Item: " + i + " -- " + fibonacci(i));
        }
    }

    public static BigInteger fibonacci(int n) {
        BigInteger[] posicion = new BigInteger[3];
        posicion[0] = BigInteger.ONE;
        posicion[1] = BigInteger.ZERO;
        return fibonacci(n, posicion);
    }

    private static BigInteger fibonacci(int n, BigInteger[] posicion) {
        if (n <= 0) {
            return BigInteger.ZERO;
        }
        if (n <= 2) {
            return BigInteger.ONE;
        } else {
            posicion[2] = posicion[1].add(posicion[0]);
            posicion[1] = posicion[0];
            posicion[0] = posicion[2];
            fibonacci(n - 1, posicion);
            return posicion[0].add(posicion[1]);
        }
    }
}
