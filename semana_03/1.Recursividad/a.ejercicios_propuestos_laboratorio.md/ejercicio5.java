public class ejercicio5 {
    private String tipo;
    private int parametro1;
    private int parametro2;

    public ejercicio5(String tipo, int parametro1, int parametro2) {
        this.tipo = tipo;
        this.parametro1 = parametro1;
        this.parametro2 = parametro2;
    }

    public void construir() {
        if (tipo.equals("Sierpinski")) {
            construirSierpinski(parametro1, parametro2);
        } else if (tipo.equals("Koch")) {
            construirKoch(parametro1, parametro2);
        } else {
            System.out.println("Fractal no válido");
        }
    }

    private void construirSierpinski(int nivel, int lado) {
        
    }

    private void construirKoch(int nivel, int lado) {
        
    }
}