import java.util.Scanner;

public class ejercicio3 {
    public static int mcm(int a, int b) {
        return a * b / mcd(a, b);
    }

    public static int mcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return mcd(b, a % b);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números: ");
        int n = scanner.nextInt();
        int[] numeros = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número " + (i + 1) + ": ");
            numeros[i] = scanner.nextInt();
        }
        scanner.close();
        int resultado = numeros[0];
        for (int i = 1; i < n; i++) {
            resultado = mcm(resultado, numeros[i]);
        }
        System.out.println("El mínimo común múltiplo de los " + n + " números es: " + resultado);
    }
}