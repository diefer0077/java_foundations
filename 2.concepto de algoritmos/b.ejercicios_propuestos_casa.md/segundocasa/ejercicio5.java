public class ejercicio5 {
    public static void main(String[] args) {
      String[] palabras = {"murcielago", "abstemio", "aceituno", "ambiguo", "anfibio", "aureola", "eucalipto", "eufonia", "euforia", "eutanasia", "ionico", "obvio", "ouija", "urinario"};
      boolean encontrado = false;
      for (String palabra : palabras) {
        if (contieneTodasLasVocales(palabra)) {
          System.out.println("La palabra " + palabra + " contiene todas las vocales");
          encontrado = true;
        }
      }
      if (!encontrado) {
        System.out.println("No se ha encontrado ninguna palabra que contenga todas las vocales");
      }
    }
  
    public static boolean contieneTodasLasVocales(String palabra) {
      return palabra.contains("a") && palabra.contains("e") && palabra.contains("i") && palabra.contains("o") && palabra.contains("u");
    }
  }