import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio15 {
    public static void main(String[] args) {
        String archivo = "palabras.txt";
        int cantidad = 0;
        
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea;
            
            while ((linea = br.readLine()) != null) {
                String palabra = linea.trim().toLowerCase();
                
                if (palabra.length() > 1 && palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                    System.out.println(palabra + " tiene la misma letra al principio y al final.");
                    cantidad++;
                }
            }
        } catch (IOException e) {
            System.err.println("Error al leer el archivo " + archivo + ": " + e.getMessage());
            System.exit(1);
        }
        
        System.out.println("Se encontraron " + cantidad + " palabras que tienen la misma letra al principio y al final.");
    }
}
