public class ejercicio14 {
    public static void main(String[] args) {
        int inicio = 1;
        int fin = 1000;
        int cantidad = 0;
        
        for (int n = inicio; n <= fin; n++) {
            int cuadrado = n * n;
            String cuadradoStr = Integer.toString(cuadrado);
            
            for (int i = 1; i < cuadradoStr.length(); i++) {
                String parte1Str = cuadradoStr.substring(0, i);
                String parte2Str = cuadradoStr.substring(i);
                
                int parte1 = Integer.parseInt(parte1Str);
                int parte2 = Integer.parseInt(parte2Str);
                
                if (parte1 + parte2 == n) {
                    System.out.println(n + " es un número de Kaprekar.");
                    cantidad++;
                    break;
                }
            }
        }
        
        System.out.println("Se encontraron " + cantidad + " números de Kaprekar en el rango de " + inicio + " a " + fin + ".");
    }
}
