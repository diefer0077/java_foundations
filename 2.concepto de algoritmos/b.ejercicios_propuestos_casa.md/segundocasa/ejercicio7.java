import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio7 {
    public static void main(String[] args) {
        String texto = "Ejemplo de palabras que tienen la misma letra repetida tres veces consecutivas: reeeesfriado, caaaaracol, perrrrro, boooote";
        Pattern patron = Pattern.compile("\\b\\w*([a-z])\\1{2}\\w*\\b");
        Matcher matcher = patron.matcher(texto);
        
        while (matcher.find()) {
            System.out.println("Palabra encontrada: " + matcher.group());
        }
    }
}
