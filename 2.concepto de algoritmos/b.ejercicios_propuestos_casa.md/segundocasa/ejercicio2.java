import java.util.Scanner;

public class ejercicio2 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Ingrese una cadena de texto: ");
    String cadena = scanner.nextLine();
    scanner.close();
    int contador = 0;
    String[] palabras = cadena.split("\\s+");
    for (String palabra : palabras) {
      int contadorVocalesConsecutivas = 0;
      for (int i = 0; i < palabra.length(); i++) {
        char letra = palabra.charAt(i);
        if ("aeiouAEIOU".indexOf(letra) >= 0) {
          contadorVocalesConsecutivas++;
          if (contadorVocalesConsecutivas > 2) {
            contador++;
            break;
          }
        } else {
          contadorVocalesConsecutivas = 0;
        }
      }
    }
    System.out.println("Número de palabras con más de dos vocales consecutivas: " + contador);
  }
}