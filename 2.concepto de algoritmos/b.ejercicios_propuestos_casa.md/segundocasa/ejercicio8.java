public class ejercicio8 {
    public static void main(String[] args) {
      for (int i = 1; i <= 100; i++) {
        int sum = 0;
        int num = i;
        while (num > 0) { 
          sum += num % 10;
          num /= 10;
        }
        if (i % sum == 0) {
          System.out.println(i + "Divisible por la suma de sus cifras");
        }
      }
    }
  }
  