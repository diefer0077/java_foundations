public class ejercicio3 {
    public static void main(String[] args) {
      int limite = 100;
      int contadorPerfectos = 0;
      int contadorFibonacci = 0;
      for (int i = 1; i <= limite; i++) {
        if (esNumeroPerfecto(i)) {
          contadorPerfectos++;
        }
        if (esNumeroDeFibonacci(i)) {
          contadorFibonacci++;
        }
      }
      System.out.println("Número de números perfectos: " + contadorPerfectos);
      System.out.println("Número de números de Fibonacci: " + contadorFibonacci);
    }
  
    public static boolean esNumeroPerfecto(int numero) {
      int sumaDivisores = 0;
      for (int i = 1; i < numero; i++) {
        if (numero % i == 0) {
          sumaDivisores += i;
        }
      }
      return sumaDivisores == numero;
    }
  
    public static boolean esNumeroDeFibonacci(int numero) {
      int a = 0;
      int b = 1;
      while (b < numero) {
        int c = a + b;
        a = b;
        b = c;
      }
      return b == numero;
    }
  }