public class ejercicio6 {
    public static void main(String[] args) {
        int i = 20; // start from 20
        boolean found = false;
        while (!found) {
            boolean divisible = true;
            for (int j = 1; j <= 20; j++) {
                if (i % j != 0) {
                    divisible = false;
                    break;
                }
            }
            if (divisible) {
                System.out.println(i + "Es divisible por todos los números del 1 al 20.");
                found = true;
            }
            i += 20;
        }
    }
}
