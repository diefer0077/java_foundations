public class ejercicio1 {
    public static void main(String[] args) {
      int[] numeros = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
      int numeroDeterminado = 15;
      boolean encontrado = false;
      for (int i = 0; i < numeros.length; i++) {
        for (int j = i + 1; j < numeros.length; j++) {
          if (numeros[i] + numeros[j] == numeroDeterminado) {
            System.out.println("Se ha encontrado un par de números cuya suma es igual a " + numeroDeterminado + ": " + numeros[i] + " y " + numeros[j]);
            encontrado = true;
          }
        }
      }
      if (!encontrado) {
        System.out.println("No se ha encontrado ningún par de números cuya suma sea igual a " + numeroDeterminado);
      }
    }
  }