public class ejercicio4 {
    public static void main(String[] args) {
      int limite = 1000;
      int contador = 0;
      for (int i = 1; i <= limite; i++) {
        if (esDivisiblePorTodos(i)) {
          contador++;
        }
      }
      System.out.println("Número de números enteros divisibles por todos los números del 1 al 10: " + contador);
    }
  
    public static boolean esDivisiblePorTodos(int numero) {
      for (int i = 1; i <= 10; i++) {
        if (numero % i != 0) {
          return false;
        }
      }
      return true;
    }
  }