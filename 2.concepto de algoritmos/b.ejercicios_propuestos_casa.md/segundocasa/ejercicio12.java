import java.util.ArrayList;

public class ejercicio12 {
    public static void main(String[] args) {
        int inicio = 1;
        int fin = 1000;
        int cantidad = 0;
        
        for (int n = inicio; n <= fin; n++) {
            ArrayList<Integer> factoresPrimos = factorizar(n);
            int sumaDigitosFactores = 0;
            
            for (int i = 0; i < factoresPrimos.size(); i++) {
                sumaDigitosFactores += sumarDigitos(factoresPrimos.get(i));
            }
            
            if (sumaDigitosFactores == sumarDigitos(n)) {
                System.out.println(n + " es un número de Smith.");
                cantidad++;
            }
        }
        
        System.out.println("Se encontraron " + cantidad + " números de Smith en el rango de " + inicio + " a " + fin + ".");
    }
    
    public static ArrayList<Integer> factorizar(int n) {
        ArrayList<Integer> factoresPrimos = new ArrayList<Integer>();
        int factor = 2;
        
        while (n > 1) {
            if (n % factor == 0) {
                factoresPrimos.add(factor);
                n /= factor;
            } else {
                factor++;
            }
        }
        
        return factoresPrimos;
    }
    
    public static int sumarDigitos(int n) {
        int suma = 0;
        
        while (n > 0) {
            suma += n % 10;
            n /= 10;
        }
        
        return suma;
    }
}
