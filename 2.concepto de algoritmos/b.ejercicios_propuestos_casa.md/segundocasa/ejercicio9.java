import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio9 {
    public static void main(String[] args) {
        String texto = "Esta es una cadena de texto con algunas palabras que tienen letras repetidas un número impar de veces: reesfriado, carpeta, perrito, abecedario, cacatúa";
        Pattern patron = Pattern.compile("\\b\\w*([a-z])\\1{1,}\\w*\\b");
        Matcher matcher = patron.matcher(texto);
        int cantidad = 0;
        
        while (matcher.find()) {
            System.out.println("Palabra encontrada: " + matcher.group());
            cantidad++;
        }
        
        System.out.println("Se encontraron " + cantidad + " palabras con una letra repetida un número impar de veces.");
    }
}
