public class ejercicio13 {
    public static void main(String[] args) {
      String[] palabras = {"hola", "mama", "papa", "casa", "solo"};
      int contador = 0;
      for (String palabra : palabras) {
        for (int i = 0; i < palabra.length(); i++) {
          char letra = palabra.charAt(i);
          int repeticiones = 0;
          for (int j = 0; j < palabra.length(); j++) {
            if (palabra.charAt(j) == letra) {
              repeticiones++;
            }
          }
          if (repeticiones > 2 && repeticiones < 5) {
            contador++;
            break;
          }
        }
      }
      System.out.println("Hay " + contador + "Letra que aparece más de dos veces pero menos de cinco veces");
    }
  }