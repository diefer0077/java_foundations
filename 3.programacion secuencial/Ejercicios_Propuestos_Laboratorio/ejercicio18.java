import java.util.Scanner;
public class ejercicio18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("el total de la compra es: ");
        double total = sc.nextDouble();
        double descuento = total * 0.15;
        double precioFinal = total - descuento; 
        System.out.println("Descuento final es: " + precioFinal);
    }
}
