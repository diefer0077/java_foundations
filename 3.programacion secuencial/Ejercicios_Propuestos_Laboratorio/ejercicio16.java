import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("primer número: ");
        double num1 = sc.nextDouble();

        System.out.print("segundo número: ");
        double num2 = sc.nextDouble();

        System.out.print("Ingrese la operación (+, -, *, /, ^): ");
        char operacion = sc.next().charAt(0);
        double resultado = 0;
        if (operacion == '+') {
            resultado = num1 + num2;
        } else if (operacion == '-') {
            resultado = num1 - num2;
        } else if (operacion == '*') {
            resultado = num1 * num2;
        } else if (operacion == '/') {
            resultado = num1 / num2;
        } else if (operacion == '^') {
            resultado = Math.pow(num1, num2);
        } else {
            System.out.println("incorecto.");
            return;
        }
        System.out.println("El resultado es: " + resultado);
    }
}


