import java.util.Scanner;
public class ejercicio4 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la longitud del primer lado: ");
        double a = sc.nextDouble();
        System.out.println("Ingrese la longitud del segundo lado: ");
        double b = sc.nextDouble();
        System.out.println("Ingrese la longitud del tercer lado: ");
        double c = sc.nextDouble();
        sc.close();
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        System.out.println("El área del triángulo es: " + area);
    
    }
}

