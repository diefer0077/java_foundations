import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa tu fecha de nacimiento (DD/MM/AAAA): ");
        String fecha = input.nextLine();

        int sumaCifras = 0;

        for (int i = 0; i < fecha.length(); i++) {
            char caracter = fecha.charAt(i);
            if (Character.isDigit(caracter)) {
                sumaCifras += Character.getNumericValue(caracter);
            }
        }

        int numeroSuerte = 0;
        while (sumaCifras > 0 || numeroSuerte > 9) {
            if (sumaCifras == 0) {
                sumaCifras = numeroSuerte;
                numeroSuerte = 0;
            }
            numeroSuerte += sumaCifras % 10;
            sumaCifras /= 10;
        }

        System.out.println("Tu número de la suerte es: " + numeroSuerte);
    }
}
