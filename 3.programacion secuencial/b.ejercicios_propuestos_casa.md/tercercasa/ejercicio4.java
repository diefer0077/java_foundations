import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa una temperatura en grados Celsius: ");
        double celsius = input.nextDouble();

        double fahrenheit = 32 + (9 * celsius / 5);

        System.out.println(celsius + " grados Celsius son equivalentes a " + fahrenheit + " grados Fahrenheit");
    }
}
