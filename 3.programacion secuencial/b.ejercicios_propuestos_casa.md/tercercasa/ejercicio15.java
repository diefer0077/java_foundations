import java.util.Scanner;

public class ejercicio15 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Introduce un número entero: ");
    int n = scanner.nextInt();
    System.out.print("Introduce el número de cifras que deseas extraer: ");
    int m = scanner.nextInt();
    int ultimasCifras = n % (int) Math.pow(10, m);
    System.out.println("Las últimas " + m + " cifras de " + n + " son: " + ultimasCifras);
  }
}