import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa un número de 5 cifras: ");
        int numero = input.nextInt();

        int cifra1 = numero / 10000;
        int cifra2 = (numero % 10000) / 1000;
        int cifra3 = (numero % 1000) / 100;
        int cifra4 = (numero % 100) / 10;
        int cifra5 = numero % 10;

        System.out.println(cifra1 + " " + cifra2 + " " + cifra3 + " " + cifra4 + " " + cifra5);
    }
}
