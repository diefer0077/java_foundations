import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa el precio del producto: $");
        double precioCosto = input.nextDouble();

        System.out.print("Ingresa el margen de beneficio (en porcentaje): ");
        double margenBeneficio = input.nextDouble() / 100.0;

        System.out.print("¿El impuesto sobre ventas aplica? (s/n): ");
        char impuesto = input.next().charAt(0);

        double precioVenta = precioCosto * (1 + margenBeneficio);

        if (impuesto == 's' || impuesto == 'S') {
            double impuestoVentas = 0.16;
            precioVenta *= (1 + impuestoVentas);
        }

        System.out.println("El precio final de venta del producto es: $" + precioVenta);
    }
}
