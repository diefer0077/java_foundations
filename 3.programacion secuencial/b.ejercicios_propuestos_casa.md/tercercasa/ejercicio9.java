import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa la longitud del primer lado: ");
        double lado1 = input.nextDouble();

        System.out.print("Ingresa la longitud del segundo lado: ");
        double lado2 = input.nextDouble();

        System.out.print("Ingresa la longitud del tercer lado: ");
        double lado3 = input.nextDouble();

        double s = (lado1 + lado2 + lado3) / 2.0;
        double area = Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));

        System.out.println("El área del triángulo es: " + area);
    }
}
