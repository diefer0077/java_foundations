import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa una velocidad en km/h: ");
        double kmh = input.nextDouble();

        double ms = kmh / 3.6;
        double mph = kmh / 1.609344;

        System.out.println(kmh + " km/h equivale a " + ms + " m/s");
        System.out.println(kmh + " km/h equivale a " + mph + " mph");
    }
}
