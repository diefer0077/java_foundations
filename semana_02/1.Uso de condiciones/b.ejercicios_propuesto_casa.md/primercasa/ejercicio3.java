public class ejercicio3 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Debe proporcionar un número entero como argumento");
            return;
        }

        int n = Integer.parseInt(args[0]);
        int factorial = 1;

        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }

        System.out.println("El factorial de " + n + " es " + factorial);
    }
}
