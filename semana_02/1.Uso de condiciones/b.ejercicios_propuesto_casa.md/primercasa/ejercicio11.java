public class ejercicio11 {
    public static void main(String[] args) {
        double montoInicial = 10.0;
        int plazoEnMeses = 20;

        double montoActual = montoInicial;
        double totalPagado = montoInicial;

        for (int i = 2; i <= plazoEnMeses; i++) {
            montoActual *= 2;
            totalPagado += montoActual;
        }

        double cuotaMensual = totalPagado / plazoEnMeses;

        System.out.printf("El monto a pagar mensualmente es: S/%.2f%n", cuotaMensual);
        System.out.printf("El total a pagar después de los %d meses es: S/%.2f%n", plazoEnMeses, totalPagado);
    }
}
