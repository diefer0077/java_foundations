import java.util.Scanner;

public class ejercicio14 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Introduce un número entero positivo: ");
    int n = scanner.nextInt();
    for (int i = 1; i <= n; i++) {
      if (i % 2 != 0) {
        System.out.print(i);
        if (i != n && i != n-1) {
          System.out.print("; ");
        }
      }
    }
  }
}