import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el primer término de la serie: ");
        double a = sc.nextDouble();
        System.out.print("Ingrese la razón común de la serie: ");
        double r = sc.nextDouble();
        System.out.print("Ingrese el número de términos a sumar: ");
        int n = sc.nextInt();
        double suma = a * (1 - Math.pow(r, n)) / (1 - r);
        System.out.println("La suma de los primeros " + n + " términos de la serie es: " + suma);
    }
}
