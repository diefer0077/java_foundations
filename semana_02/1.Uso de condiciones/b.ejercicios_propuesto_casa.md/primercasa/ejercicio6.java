import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números: ");
        int n = sc.nextInt();
        int[] numeros = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número #" + (i + 1) + ": ");
            numeros[i] = sc.nextInt();
        }

        for (int i = 1; i < n; i++) {
            int j = i;
            while (j > 0 && numeros[j - 1] > numeros[j]) {
                int temp = numeros[j];
                numeros[j] = numeros[j - 1];
                numeros[j - 1] = temp;
                j--;
            }
        }

        System.out.println("Los números ordenados son:");
        for (int i = 0; i < n; i++) {
            System.out.print(numeros[i] + " ");
        }
    }
}
