public class ejercicio15 {
    public static void main(String[] args) {
        double prestamo = 5000;
        double tasaInteresMensual = 0.016;
        int meses = 18;
        double saldo = prestamo;
        double pagoMensual = prestamo / meses;
        
        System.out.println("Mes\tPago\tSaldo");
        
        for (int i = 1; i <= meses; i++) {
            double interesMensual = saldo * tasaInteresMensual;
            double pagoInteres = interesMensual;
            double pagoCapital = pagoMensual - pagoInteres;
            saldo -= pagoCapital;
            
            System.out.printf("%d\t%.2f\t%.2f\n", i, pagoMensual, saldo);
        }
    }
}
