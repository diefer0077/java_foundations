import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce una palabra: ");
        String palabra = scanner.nextLine();
        int i = 0;
        while (i < palabra.length()) {
            System.out.println("Letra " + (i+1) + ": " + palabra.substring(i, i+1));
            i++;
        }
    }
}
