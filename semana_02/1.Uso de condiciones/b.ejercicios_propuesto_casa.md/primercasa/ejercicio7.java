import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de términos de la sucesión de Ulam a imprimir: ");
        int n = sc.nextInt();
        int[] ulam = new int[n];
        ulam[0] = 1;
        int i = 1;
        while (i < n) {
            int candidato = ulam[i - 1] + 1;
            while (!esSumaDeDosDistintos(ulam, i, candidato)) {
                candidato++;
            }
            ulam[i] = candidato;
            i++;
        }
        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int j = 0; j < n; j++) {
            System.out.print(ulam[j] + " ");
        }
    }

    static boolean esSumaDeDosDistintos(int[] a, int n, int x) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] + a[j] == x && a[i] != a[j]) {
                    return true;
                }
            }
        }
        return false;
    }
}
