import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {

        double[] valores = leerArchivo("datos.txt");

        double promedio = 0;
        double moda = 0;
        double varianza = 0;
        for (int i = 0; i < valores.length; i++) {
            promedio += valores[i];
            if (valores[i] == moda) {

            } else if (ocurrencias(valores, valores[i]) > ocurrencias(valores, moda)) {
                moda = valores[i];
            }
            varianza += Math.pow(valores[i] - promedio / valores.length, 2);
        }
        promedio /= valores.length;
        double desviacion = Math.sqrt(varianza / valores.length);

        System.out.println("Valores: " + Arrays.toString(valores));
        System.out.printf("Promedio: %.2f%n", promedio);
        System.out.println("Moda: " + moda);
        System.out.printf("Desviación estándar: %.2f%n", desviacion);
    }

    public static double[] leerArchivo(String nombreArchivo) {
        try {
            Scanner sc = new Scanner(new File(nombreArchivo));
            int n = 0;
            while (sc.hasNextDouble()) {
                n++;
                sc.nextDouble();
            }
            double[] valores = new double[n];
            sc = new Scanner(new File(nombreArchivo));
            for (int i = 0; i < n; i++) {
                valores[i] = sc.nextDouble();
            }
            sc.close();
            return valores;
        } catch (FileNotFoundException e) {
            System.out.println("El archivo no existe.");
            return new double[0];
        }
    }

    public static int ocurrencias(double[] arreglo, double valor) {
        int contador = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == valor) {
                contador++;
            }
        }
        return contador;
    }
}
