import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int num = sc.nextInt();
        int sumaDigitos = 0;
        int aux = num;
        while (aux > 0) {
            sumaDigitos += aux % 10;
            aux /= 10;
        }
        if (num % sumaDigitos == 0) {
            System.out.println(num + " es un número de Harshad");
        } else {
            System.out.println(num + " no es un número de Harshad");
        }
    }
}
