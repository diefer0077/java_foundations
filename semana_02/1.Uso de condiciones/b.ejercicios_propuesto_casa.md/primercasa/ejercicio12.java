import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de números primos que desea mostrar: ");
        int cantidadPrimos = sc.nextInt();

        int numeroActual = 2;
        int primosEncontrados = 0;

        while (primosEncontrados < cantidadPrimos) {
            if (esPrimo(numeroActual)) {
                System.out.print(numeroActual + " ");
                primosEncontrados++;
            }
            numeroActual++;
        }
    }

    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }

        return true;
    }
}
