import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner imput = new Scanner(System.in);
        System.out.print("Introduzca el valor de N: ");
        int n = imput.nextInt();

        int sum = 0;
        int num = 2;
        int count = 1;

        do {
            if (num % 2 == 0) {
                sum += num;
                count++;
            }
            num++;
        } while (count <= n);

        System.out.println("la primera suma es: "+ n);
        System.out.println("Los números pares son: " + sum);
    }
}
