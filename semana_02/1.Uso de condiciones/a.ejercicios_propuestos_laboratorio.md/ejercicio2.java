import java.util.Scanner;

public class ejercicio2 {
   public static void main(String[] args) {

      Scanner sc = new Scanner(System.in);
      System.out.print("escriba n: ");
      int n = sc.nextInt();

      int sum = 0;
      for (int i = 1; i <= 2 * n; i += 2) {
         sum += i;
      }

      System.out.println("la suma primero " + n + " Los números impares son: " + sum);
   }
}
