public class Ejercicio1 {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        int contador = 1;
        for (int fila = 0; fila < 7; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                matriz[fila][columna] = contador;
                contador++;
            }
        }
        int[][] matriz_rotada = new int[7][7];
        for (int fila = 0; fila < 7; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                matriz_rotada[columna][6 - fila] = matriz[fila][columna];
            }
        }

        for (int fila = 0; fila < 7; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                System.out.print(matriz_rotada[fila][columna] + " ");
            }
            System.out.println();
        }
    }
}
