import java.util.HashMap;
import java.util.Map;
public class Ejercicio10 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 4, 5, 5, 5, 6};
        int mode = findMode(arr);
        System.out.println("Mode: " + mode);
    }
    public static int findMode(int[] arr) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            int key = arr[i];
            if (frequencyMap.containsKey(key)) {
                frequencyMap.put(key, frequencyMap.get(key) + 1);
            } else {
                frequencyMap.put(key, 1);
            }
        }

        int mode = 0;
        int maxFrequency = 0;
        for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
            int frequency = entry.getValue();
            if (frequency > maxFrequency) {
                maxFrequency = frequency;
                mode = entry.getKey();
            }
        }
        return mode;
    }
}
