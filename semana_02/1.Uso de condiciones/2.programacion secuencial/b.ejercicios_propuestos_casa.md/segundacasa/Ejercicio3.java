public class Ejercicio3{
    public static void main(String[] args) {
        // Crear la matriz de 5x5
        int[][] matriz1 = new int[][] {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
        };
        // Crear la matriz de 3x3
        int[][] matriz2 = new int[][] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int[][] resultado = new int[5][5];
        for (int fila = 0; fila < 5; fila++) {
            for (int columna = 0; columna < 5; columna++) {
                int suma = 0;
                for (int i = 0; i < 3; i++) {
                    suma += matriz1[fila][i] * matriz2[i][columna];
                }
                resultado[fila][columna] = suma;
            }
        }
        for (int fila = 0; fila < 5; fila++) {
            for (int columna = 0; columna < 5; columna++) {
                System.out.print(resultado[fila][columna] + " ");
            }
            System.out.println();
        }
    }
}
