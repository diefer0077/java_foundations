import java.util.Scanner;
public class Ejercicio15 {
    public static void main(String[] args) {
       int[] numeros = {5, 2, 8, 3, 1, 7}; 
       int contadorPares = 0;
       for (int i = 0; i < numeros.length; i++) {
          if (numeros[i] % 2 == 0) {
             contadorPares++;
          }
       }
       System.out.println("Hay " + contadorPares + " números pares en el arreglo.");
    }
 }
 