public class Ejercicio12 {
    public static void main(String[] args) {
       int[] numeros = {5, 2, 8, 3, 1, 7}; 
       int maximo = numeros[0]; 
       for (int i = 1; i < numeros.length; i++) {
          if (numeros[i] > maximo) {
             maximo = numeros[i];
          }
       }
       System.out.println("El número mayor es: " + maximo);
    }
 }
 