public class Ejercicio5 {
    public static void main(String[] args) {
        // Crear el arreglo de enteros
        int[] arreglo = {3, 4, -1, 0, 6, 2, 3};
        int[] lis = encontrarLIS(arreglo);
        System.out.print("Subarreglo máximo creciente: ");
        for (int elemento : lis) {
            System.out.print(elemento + " ");
        }
    }
    public static int[] encontrarLIS(int[] arreglo) {
        int n = arreglo.length;
        int[] lis = new int[n];

        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (arreglo[i] > arreglo[j] && lis[i] < lis[j] + 1) {
                    lis[i] = lis[j] + 1;
                }
            }
        }
        int maxLength = 0;
        for (int i = 0; i < n; i++) {
            if (lis[i] > maxLength) {
                maxLength = lis[i];
            }
        }
        int[] resultado = new int[maxLength];
        int index = maxLength - 1;
        for (int i = n - 1; i >= 0 && index >= 0; i--) {
            if (lis[i] == maxLength && (index == maxLength - 1 || arreglo[i] < resultado[index + 1])) {
                resultado[index] = arreglo[i];
                maxLength--;
                index--;
            }
        }

        return resultado;
    }
}
