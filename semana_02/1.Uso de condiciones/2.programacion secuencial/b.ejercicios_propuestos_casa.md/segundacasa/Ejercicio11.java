public class Ejercicio11 {
    public static void main(String[] args) {
        int[] arr = {3, 5, 7, 1, 8, 4};
        double average = findAverage(arr);
        System.out.println("Average: " + average);
    }

    public static double findAverage(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        double average = (double) sum / arr.length;
        return average;
    }
}
