import java.util.Arrays;
import java.util.Comparator;
public class Ejercicio6{
    public static void main(String[] args) {
        String[] arreglo = {"León", "Tigre", "Rinoceronte", "Gepardo", "Rata"};
        Arrays.sort(arreglo, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });

        for (String elemento : arreglo) {
            System.out.println(elemento);
        }
    }
}
