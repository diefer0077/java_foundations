import java.util.Arrays;
public class Ejercicio14{
   public static void main(String[] args) {
      int[] numeros = {1, 2, 2, 2, 3, 3, 4, 5, 5, 6}; 
      
      int numeroBuscado = 2; 
      
      int indiceInicial = Arrays.binarySearch(numeros, numeroBuscado); 
            if (indiceInicial < 0) {
         System.out.println("El número " + numeroBuscado + " no está en el arreglo.");
         return;
      }
      
      int contador = 1; 
      for (int i = indiceInicial + 1; i < numeros.length; i++) {
         if (numeros[i] == numeroBuscado) {
            contador++;
         } else {
            break; 
         }
      }
      
      System.out.println("El número " + numeroBuscado + " aparece " + contador + " veces en el arreglo.");
   }
}
