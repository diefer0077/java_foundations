import java.util.Random;
public class Ejercicio2{
    public static void main(String[] args) {
        // Crear una matriz de 7x7
        int[][] matriz = new int[7][7];
        Random random = new Random();
        for (int fila = 0; fila < 7; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                matriz[fila][columna] = random.nextInt(100) + 1;
            }
        }
        for (int fila = 0; fila < 7; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                System.out.print(matriz[fila][columna] + " ");
            }
            System.out.println();
        }
    }
}
