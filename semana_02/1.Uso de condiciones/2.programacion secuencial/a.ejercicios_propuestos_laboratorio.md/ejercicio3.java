import java.util.Scanner;


public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        double sumaNotas = 0;

        System.out.println("Ingrese el número de estudiantes: ");
        n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.println("Ingrese la nota del estudiante " + (i+1) + ": ");
            sumaNotas += sc.nextDouble();
        }

        double promedioNotas = sumaNotas / n;

        System.out.println("El promedio de notas de los estudiantes es: " + promedioNotas);
    }
}