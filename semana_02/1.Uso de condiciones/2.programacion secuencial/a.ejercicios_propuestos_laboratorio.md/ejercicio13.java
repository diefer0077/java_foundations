import java.util.Scanner;
public class ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;

        do {
            System.out.print("Ingresa un número entre 1 y 7: ");
            num = sc.nextInt();
        } while (num < 1 || num > 7);

        if (num == 1) {
            System.out.println("Hoy aprenderemos sobre programación");
        } else if (num == 2) {
            System.out.println("¿Qué tal tomar un curso de marketing digital?");
        } else if (num == 3) {
            System.out.println("Hoy es un gran día para comenzar a aprender de diseño");
        } else if (num == 4) {
            System.out.println("¿Y si aprendemos algo de negocios online?");
        } else if (num == 5) {
            System.out.println("Veamos un par de clases sobre producción audiovisual");
        } else if (num == 6) {
            System.out.println("Tal vez sea bueno desarrollar una habilidad blanda");
        } else if (num == 7) {
            System.out.println("Yo decido distraerme programando");
        }
    }
}