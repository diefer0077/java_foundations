class ejercicio4{
    public static int encontrarMaximo(int[] arreglo) {
        int maximo = arreglo[0]; // asumimos que el primer elemento es el máximo
    
        // Iteramos sobre los elementos del arreglo:
        for (int i = 1; i < arreglo.length; i++) {
            // Si el elemento actual es mayor que el máximo,
            // actualizamos el valor máximo:
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
                int[] miArreglo = {3, 7, 2, 8, 1, 4};
                int maximo1 = encontrarMaximo(miArreglo);
                System.out.println("El máximo es: " + maximo);
            }
        }
        return maximo;
    }
}