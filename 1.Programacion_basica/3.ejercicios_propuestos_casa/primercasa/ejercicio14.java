import java.math.BigDecimal;

public class ejercicio14 {
  public static void main(String[] args) {
    BigDecimal numero1 = new BigDecimal("12345678901234567890.123456789");
    BigDecimal numero2 = new BigDecimal("98765432109876543210.987654321");
    BigDecimal suma = numero1.add(numero2);
    System.out.println("La suma de los números es: " + suma);
  }
}