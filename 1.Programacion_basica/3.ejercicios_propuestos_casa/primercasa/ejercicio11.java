import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio11 {
    public static void main(String[] args) {
        String cadena = "La casa es roja y tiene un jardín";
        Pattern patron = Pattern.compile("\\b[a-z]{4}\\b"); // patrón para encontrar palabras de 4 letras
        Matcher matcher = patron.matcher(cadena);
        
        while (matcher.find()) {
            System.out.println("Palabra encontrada: " + matcher.group());
        }
    }
}
