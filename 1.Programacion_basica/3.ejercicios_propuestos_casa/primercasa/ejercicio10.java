import java.util.regex.Pattern;

public class ejercicio10 {
  public static void main(String[] args) {
    String expresionRegular = "\\d+";
    Pattern patron = Pattern.compile(expresionRegular);
    String texto = "La edad de Jorge es 25";
    boolean resultado = patron.matcher(texto).find();
    System.out.println("¿El texto contiene un número? " + resultado);
  }
}