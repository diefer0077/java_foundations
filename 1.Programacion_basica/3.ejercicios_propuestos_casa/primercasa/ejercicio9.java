public class ejercicio9 {
    public static void main(String[] args) {
        double numero1 = 3.1416;
        double numero2 = 2.7182;
        
        double raiz = Math.sqrt(numero1);
        System.out.println("La raíz cuadrada de " + numero1 + " es " + raiz);
        
        double potencia = Math.pow(numero1, 2);
        System.out.println(numero1 + " elevado al cuadrado es " + potencia);
        
        double redondeo = Math.round(numero2);
        System.out.println("El redondeo de " + numero2 + " es " + redondeo);
        
        double absoluto = Math.abs(numero1 - numero2);
        System.out.println("El valor absoluto de la diferencia entre " + numero1 + " y " + numero2 + " es " + absoluto);
    }
}
