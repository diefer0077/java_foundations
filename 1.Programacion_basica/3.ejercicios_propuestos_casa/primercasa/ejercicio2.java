import java.util.HashMap;

public class ejercicio2 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<>();
        personas.put("Juan", 30);
        personas.put("María", 25);
        personas.put("Pedro", 40);
        personas.put("Lucía", 20);
        personas.put("Ana", 35);

        for (String nombre : personas.keySet()) {
            System.out.println(nombre + " tiene " + personas.get(nombre) + " años");
        }
    }
}
